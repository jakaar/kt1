public class Sheep {

   enum Animal {sheep, goat}
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      // Check that there arent only 1 or 0 elements in the array
      if (animals.length < 2) return;
      // Find how many goats are in the array
      int goat = 0;
      for (int i = 0; i < animals.length; i++) {
         if (animals[i] == Animal.goat){
            goat++;
         }
      }
      // Change the amount of elements to goat that i have counted and others to sheep
      for (int i = 0; i < animals.length; i++) {
         if (i < goat){
            animals[i] = Animal.goat;
         }else {
            animals[i] = Animal.sheep;
         }
      }
   }
   // https://enos.itcollege.ee/~jpoial/algoritmid/Loendamismeetod.html
}
